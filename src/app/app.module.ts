import { HttpModule } from '@angular/http';
import { MessagesService } from './messages.service';
import { UsersService } from './users.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { UsersComponent } from './users/users.component';
import { MessagesFormComponent } from './messages/messages-form/messages-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NavigationComponent } from './navigation/navigation.component';
import {RouterModule} from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';
import { MessageComponent } from './messages/message/message.component';
import { UserComponent } from './users/user/user.component';
import { MessageFormComponent } from './messages/message-form/message-form.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { environment } from '../environments/environment';
import { MessagesfComponent } from './messagesf/messagesf.component';

@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    UsersComponent,
    MessagesFormComponent,
    NavigationComponent,
    NotFoundComponent,
    MessageComponent,
    UserComponent,
    MessageFormComponent,
    MessagesfComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      {path: '', component: MessagesComponent},
      {path: 'users', component: UsersComponent},
      {path: 'message/:id', component: MessageComponent},
      {path: 'message-form/:id', component: MessageFormComponent},
      {path: 'messagesf', component: MessagesfComponent},
      {path: '**', component: NotFoundComponent}
])
  ],
  providers: [
    MessagesService,
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }