import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';
import { environment } from '../environments/environment';

@Injectable()
export class MessagesService {
  http:Http;
  getMessages(){
    //return ['a','b','c'];
    //get messages from the SLIM rest API (Don't say DB)
    //return  this.http.get('http://localhost/slimapp/messages');
     return this.http.get(environment.url + 'messages');
    }
  
    //השרת שמתחבר לפיירבייס
  getMessagesFire(){
      //הוויליו מייצר את האובזווריבל
      return this.db.list('/messages').valueChanges();
     }

  getMessage(id){
    return  this.http.get('http://localhost/slimapp/messages/'+id);
  }

  postMessage (data){//input:Json, output: (key,value) //send the data to the server
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('message',data.message)
    return this.http.post('http://localhost/slimapp/messages', params.toString(), options);
  }
  putMessage(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('message',data.message);
    return this.http.put('http://localhost/slimapp/messages/'+ key,params.toString(), options);
  }

  deleteMessage(key){
    return this.http.delete('http://localhost/slimapp/messages/'+ key);
  }

  constructor(http:Http, private db:AngularFireDatabase) { 
    this.http = http;
  }
}